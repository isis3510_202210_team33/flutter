import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:parchapp/Screens/Profile/profile_page.dart';
import 'package:parchapp/Screens/dashboard.dart';

import '../Screens/Map/map_page.dart';

const icon = Icons.person;

class NavigationBarWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) => Container(
        margin: const EdgeInsets.symmetric(vertical: 12, horizontal: 16),
        child: Container(
          height: 60,
          decoration: BoxDecoration(
            boxShadow: [
              BoxShadow(color: Colors.grey),
            ],
            color: Theme.of(context).primaryColor,
            borderRadius: BorderRadius.circular(10),
          ),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              IconButton(
                icon: Icon(Icons.map),
                onPressed: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(builder: (context) => MapPage()),
                  );
                },
              ),
              IconButton(
                icon: Icon(Icons.star),
                color: Colors.white,
                onPressed: () {
                  Navigator.of(context).popUntil((route)=>route.isFirst,
                  );
                },
              ),
              IconButton(
                icon: Icon(icon),
                onPressed: () {
                  Navigator.of(context).push(
                    MaterialPageRoute(builder: (context) => ProfilePage()),
                  );
                },
              ),
            ],
          ),
        ),
      );
}
