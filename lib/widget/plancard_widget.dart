import 'package:flutter/material.dart';

import '../model/plan.dart';

class PlanCard extends StatelessWidget {
  const PlanCard({Key? key, required this.plan, required this.imageURL, required this.bigCardSize}) : super(key: key);
  final Plan plan;
  final String imageURL;
  final bool bigCardSize;



  @override
  Widget build(BuildContext context) {
    double heightValue = 150;
    double imageHeight = 80;
    double planNombreSize = 20;
    var descripcionBox = Container(child: Text(
      plan.descripcion,
      style: TextStyle(
        color: Colors.white,
        fontSize: 12,
      ),
    ),);

    var horaIniciaBox = Container(
      child: Text(plan.horaInicia,
          style: TextStyle(
            color: Colors.white,
          )),
    );

    if(!bigCardSize){
      heightValue = 60;
      imageHeight = 50;
      planNombreSize = 15;
      descripcionBox = Container();
      horaIniciaBox = Container(child: SizedBox(height: 0,));
    }
    return Container(

      margin: const EdgeInsets.only(left: 20, right: 20, bottom: 20),

      height: heightValue,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(20),
        color: Theme.of(context).scaffoldBackgroundColor,
        boxShadow: [
          BoxShadow(
            color: Theme.of(context).primaryColor,
            offset: Offset(0.0, 1.0), //(x,y)
            blurRadius: 8.0,
          ),
        ],
      ),
      child: Stack(

        children: [
          Stack(
            children: [
              Positioned.fill(
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(20),

                ),
              ),
            ],
          ),
          Positioned(

            bottom: 0,
            left: 0,
            right: 0,
            child: Container(
              height: heightValue,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(20),
                    bottomRight: Radius.circular(20),
                    topLeft: Radius.circular(20),
                    topRight: Radius.circular(20)),
                gradient: LinearGradient(
                    begin: Alignment.centerLeft,
                    end: Alignment.centerRight,
                    colors: [
                      Theme.of(context)
                          .primaryColor
                          .withOpacity(1),
                      Theme.of(context)
                          .primaryColor
                          .withOpacity(0.8)
                    ]),
              ),
            ),
          ),
          Container(
              alignment: Alignment.center,

              child: Container(
                alignment: Alignment.bottomCenter,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment:
                  MainAxisAlignment.spaceAround,
                  children: [
                    SizedBox(
                      child: IconButton(
                        icon: Icon(
                          Icons.favorite_border,
                          color: Colors.white,
                          size: 20,
                        ),
                        onPressed: () {},
                      ),
                    ),
                    SizedBox(
                      width: 170,
                      child: Padding(
                        padding: const EdgeInsets.only(
                            top: 20, bottom: 20),
                        child: Column(
                          mainAxisAlignment:
                          MainAxisAlignment.spaceBetween,
                          crossAxisAlignment:
                          CrossAxisAlignment.start,
                          children: [
                            Text(
                              plan.nombre,
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: planNombreSize,
                              ),
                            ),
                            // SizedBox(height: 5),
                            descripcionBox,
                            // Text(
                            //   "Creado por " +
                            //       plan.propietario,
                            //   style: TextStyle(
                            //     color: Colors.white,
                            //     fontSize: 12,
                            //   ),
                            // ),
                          ],
                        ),
                      ),
                    ),
                    Column(
                      mainAxisAlignment:
                      MainAxisAlignment.spaceAround,
                      children: [
                        SizedBox(
                          //La sized box correspondiente a la imagen del lugar
                          width: 80,
                          height: imageHeight,
                          child: Stack(
                            children: [
                              Positioned.fill(
                                child: ClipRRect(
                                  borderRadius:
                                  BorderRadius.circular(20),
                                  child: Image.network(
                                    imageURL,
                                    fit: BoxFit.cover,
                                  ),
                                ),
                              )
                            ],
                          ),
                        ),
                        horaIniciaBox,
                      ],
                    )
                  ],
                ),
              ))
        ],
      ),
    );
  }
}
