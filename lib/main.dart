import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:parchapp/themes.dart';
import 'package:parchapp/utils/user_preferences.dart';
import 'firebase_options.dart';
import 'package:provider/provider.dart';
import 'Screens/Login/login.dart';
import 'Screens/dashboard.dart' as maindashboard;
import 'package:parchapp/globals.dart' as globals;

void main() async {

  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp(
    options: DefaultFirebaseOptions.currentPlatform,
  );

  await UserPreferences.init();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) => ChangeNotifierProvider(
        create: (context) => ThemeProvider(),
        builder: (context, _) {
          final themeProvider = Provider.of<ThemeProvider>(context);

          return MaterialApp(
            themeMode: themeProvider.themeMode,
            theme: MyThemes.lightTheme,
            darkTheme: MyThemes.darkTheme,
            title: 'ParchApp',
            home: const Login(),
          );
        },
      );
}


//
// class Login extends StatelessWidget {
//   const Login({Key? key}) : super(key: key);
//
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       body: Center(
//         child: ElevatedButton(
//           child: Text("Login"),
//           onPressed: () {
//             _goToDashboard(context);
//           },
//         ),
//       ),
//     );
//   }
//
//   void _goToDashboard(BuildContext context) {
//     final route = MaterialPageRoute(builder: (BuildContext context){
//       return Dashboard(
//           email: "prueba2@gmail.com"
//       );
//     });
//     Navigator.of(context).push(route);
//   }
// }
