import 'dart:convert';

import 'package:connectivity/connectivity.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';

import '../../model/user.dart';
import '../../widget/navigationbar_widget.dart';
import 'package:parchapp/globals.dart' as globals;

class CrearPlanPage extends StatefulWidget {
  const CrearPlanPage({Key? key}) : super(key: key);
  @override
  _CrearPlanPageState createState() => _CrearPlanPageState();
}

class _CrearPlanPageState extends State<CrearPlanPage> {

  final url = "http://44.198.195.210:8080/planes";
  final urlGrupos = "http://44.198.195.210:8080/clientes/${globals.sesion.getIdUser().toString()}/grupos";
  final urlCliente = "http://44.198.195.210:8080/clientes/${globals.sesion.getIdUser().toString()}";
  var hora;
  var nombre;
  var descripcion;
  var grupo;
  String dropdownvalue = 'Seleccionar grupo';
  var groupsArray = [];
  // List of items in our dropdown menu
  var items = [
    'Seleccionar grupo'
  ];


  Future<http.Response> postData() {
    String grupoSeleccionado = '1';
    if(nombre==""||descripcion==null){
      nombre = "Defecto";
    }
    if(descripcion==""||descripcion==null){
      descripcion= "Defecto";
    }
    if(hora==""||hora==null){
      hora="05:00";
    }
    else{
      hora = "${hora.hour}:${hora.minute}";
    }
    for(int i = 0; i < groupsArray.length; i++){
      if(groupsArray[i]['fields']['nombre']==dropdownvalue){
        grupoSeleccionado=groupsArray[i]['pk'].toString();
      }
    }
      // print(nombre);
      // print(descripcion);
      // print(hora);
      // print(dropdownvalue);
      return http.post(
        Uri.parse(url),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode(<String, String>{
          'nombre': nombre,
          'descripcion': descripcion,
          'presupuesto': '10000',
          'horaInicia': hora,
          'grupo': grupoSeleccionado,
          'lugar': '1',
          'evento': '1'
        }),
      );
      //   if(nombre!=""&&descripcion!=""&&hora!=null)
      //     {
      //       var postBody = {
      //         "nombre": nombre,
      //         "descripcion": descripcion,
      //         "presupuesto": "10000",
      //         "horaInicia": hora.hour.toString() + ":" + hora.minute.toString(),
      //         "grupo": "511"
      //       };
      //       final response = await post(Uri.parse(url), body: postBody);
      //       print(postBody);
      //       print(response.body);
      //     }
      //
      // }catch(e){
      //   print("error: ");
      //   print(e);
      // }


  }

  void fetchUserGroups() async{
    var connectivity = await Connectivity().checkConnectivity();

    if (connectivity == ConnectivityResult.wifi ||
        connectivity == ConnectivityResult.mobile) {
      if (groupsArray.isEmpty) {
        try {
          String itemURL = urlGrupos;
          final response = await get(Uri.parse(itemURL));
          var jsonData = jsonDecode(response.body);
          for(int i = 0; i<jsonData.length; i++){
            setState(() {
              groupsArray.add(jsonData[i]);
              items.add(jsonData[i]["fields"]["nombre"]);
            });
          }
          // print(groupsArray);


          //
        } catch (err) {
          print("error fetching data: $err");
        }
      }
    } else {}
  }

 void updatePlans() {
    try{
      User activeUser = globals.sesion.getInstanceUsuario();
      final response = http.put(
        Uri.parse(urlCliente),
        headers: <String, String>{
          'Content-Type': 'application/json; charset=UTF-8',
        },
        body: jsonEncode(<String, String>{
          'username': activeUser.userName,
          'nombre': activeUser.name,
          'password': activeUser.password,
          'correo': activeUser.email,
          'fechaNacimiento': activeUser.birthday,
          'telefono': '1010',
          'imagen': activeUser.imagePath,
          'planes': '[1,2,3]',
        }),
      );

    } catch (err) {
    print("error fetching data: $err");

    }
  }

  _fetchGroups(){
    fetchUserGroups();
  }

  void submitData(){
    postData();
    updatePlans();
  }

  @override
  void initState() {
    super.initState();
    _fetchGroups();
  }


    @override
    Widget build(BuildContext context) {
      return Scaffold(
        resizeToAvoidBottomInset: false,
        body: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              "Crea tu plan",
              style: TextStyle(fontSize: 30),
            ),
            Container(
              margin: EdgeInsets.all(20),
              height: 300,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20),
                color: Theme
                    .of(context)
                    .scaffoldBackgroundColor,
                boxShadow: [
                  BoxShadow(
                    color: Theme
                        .of(context)
                        .primaryColor,
                    offset: Offset(0.0, 1.0), //(x,y)
                    blurRadius: 8.0,
                  ),
                ],
              ),
              child: Stack(
                children: [
                  Positioned.fill(
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(20),
                      child: Image.asset(
                        'assets/RU-min.jpg',
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                  Positioned(
                    bottom: 0,
                    left: 0,
                    right: 0,
                    child: Container(
                      height: 300,
                      decoration: BoxDecoration(
                        borderRadius: const BorderRadius.only(
                            bottomLeft: Radius.circular(20),
                            bottomRight: Radius.circular(20),
                            topLeft: Radius.circular(20),
                            topRight: Radius.circular(20)),
                        gradient: LinearGradient(
                            begin: Alignment.centerLeft,
                            end: Alignment.centerRight,
                            colors: [
                              Theme
                                  .of(context)
                                  .primaryColor
                                  .withOpacity(1),
                              Theme
                                  .of(context)
                                  .primaryColor
                                  .withOpacity(0.8)
                            ]),
                      ),
                    ),
                  ),
                  Container(
                      alignment: Alignment.center,
                      child: Container(
                        alignment: Alignment.bottomCenter,
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            SizedBox(
                              width: 170,
                              child: Padding(
                                padding:
                                const EdgeInsets.only(top: 20, bottom: 20),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    TextField(
                                      onChanged: (text) {
                                        setState(() {
                                          nombre = text;
                                        });
                                      },
                                      decoration: InputDecoration(
                                        border: OutlineInputBorder(),
                                        labelText: 'Nombre',
                                      ),
                                    ),
                                    const SizedBox(height: 20),
                                    TextField(
                                      onChanged: (desc) {
                                        setState(() {
                                          descripcion = desc;
                                        });
                                      },
                                      keyboardType: TextInputType.multiline,
                                      minLines:
                                      2,
                                      maxLines: 2,
                                      decoration: InputDecoration(
                                        border: OutlineInputBorder(),
                                        labelText: 'Descripcion',
                                      ),
                                    ),
                                    const SizedBox(height: 10),
                                    DropdownButton(
                                      // Initial Value
                                      value: dropdownvalue,

                                      // Down Arrow Icon
                                      icon: const Icon(
                                          Icons.keyboard_arrow_down),

                                      // Array list of items
                                      items: items.map((String items) {
                                        return DropdownMenuItem(
                                          value: items,
                                          child: Text(items),
                                        );
                                      }).toList(),
                                      // After selecting the desired option,it will
                                      // change button value to selected value
                                      onChanged: (String? newValue) {
                                        setState(() {
                                          dropdownvalue = newValue!;
                                        });
                                      },
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              crossAxisAlignment: CrossAxisAlignment.center,
                              children: [
                                SizedBox(
                                  //La sized box correspondiente a la imagen del lugar
                                    width: 80,
                                    height: 80,
                                    child: Stack(
                                      children: [
                                        Positioned.fill(
                                          child: ClipRRect(
                                            borderRadius:
                                            BorderRadius.circular(20),
                                            child: Image.asset(
                                              'assets/RU-min.jpg',
                                              fit: BoxFit.cover,
                                            ),
                                          ),
                                        ),
                                      ],
                                    )),
                                const SizedBox(
                                  height: 20,
                                ),
                                IconButton(
                                  icon: const Icon(
                                    Icons.access_time,
                                    color: Colors.white,
                                    size: 50,
                                  ),
                                  onPressed: () async {
                                    var horaSel = await showTimePicker(
                                      context: context,
                                      initialTime:
                                      const TimeOfDay(hour: 7, minute: 15),
                                    );
                                    setState(() {
                                      hora = horaSel;
                                      // print(hora.hour.toString()+":"+hora.minute.toString());
                                    });
                                  },
                                ),

                              ],
                            ),
                          ],
                        ),
                      ))
                ],
              ),
            ),
            SizedBox(
              width: 100,
              height: 100,
              child: IconButton(
                icon: Icon(
                  Icons.add_circle,
                  color: Theme
                      .of(context)
                      .primaryColor,
                  size: 70,
                ),
                onPressed: () {
                  submitData();
                  Navigator.pop(context);
                },
              ),
            ),
            SizedBox(
              height: 50,
            ),
          ],
        ),
      );
    }
  }
