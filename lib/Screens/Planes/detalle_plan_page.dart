import 'package:flutter/material.dart';

class DetallePlan extends StatefulWidget {
  const DetallePlan({Key? key, required this.nombrePlan, required this.descripcionPlan, required this.urlImagenPlan, required this.grupoPlan, required this.horaIniciaPlan, required this.lugarPlan}) : super(key: key);
  final String nombrePlan;
  final String descripcionPlan;
  final String urlImagenPlan;
  final String grupoPlan;
  final String horaIniciaPlan;
  final String lugarPlan;


  @override
  State<DetallePlan> createState() => _DetallePlanState();
}

class _DetallePlanState extends State<DetallePlan> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        width: MediaQuery.of(context).size.width,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          children: [
            Text(widget.nombrePlan,
              style: TextStyle(fontWeight: FontWeight.bold, fontSize: 50, color: Theme.of(context).primaryColor),),
            SizedBox(height: 20,),
            Text(widget.descripcionPlan),
            Text("ID del grupo: ${widget.grupoPlan}"),
            Text("Hora de inicio del plan: ${widget.horaIniciaPlan.substring(11,16)}"),
            Text("ID del lugar: ${widget.lugarPlan}"),

          ],
        ),
      ),
    );
  }
}
