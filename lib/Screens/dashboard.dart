import 'dart:collection';
import 'dart:convert';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:http/http.dart';
import 'package:parchapp/Screens/Planes/crear_plan_page.dart';
import 'package:parchapp/Screens/Planes/detalle_plan_page.dart';
import 'package:parchapp/model/lugar.dart';
import 'package:parchapp/utils/planes_mocks.dart';
import 'package:parchapp/widget/navigationbar_widget.dart';
import 'package:connectivity/connectivity.dart';
import '../model/plan.dart';
import '../widget/plancard_widget.dart';
import 'package:parchapp/globals.dart' as globals;

class Dashboard extends StatefulWidget {
  const Dashboard({Key? key}) : super(key: key);

  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  HashMap<int, Plan> _cachedPlanes = HashMap<int, Plan>();
  HashMap<int, Lugar> _cachedLugares = HashMap<int, Lugar>();
  ScrollController _scrollController = ScrollController();
  var componente = Column();
  bool viewType = false;

    final urlPlanes = "http://44.198.195.210:8080/clientes/${globals.sesion.getIdUser().toString()}/planes";
  final urlLugares = "http://44.198.195.210:8080/lugares";

  void changeViewType(){
    viewType = !viewType;
    setState(() {

    });
  }


  void fetchPlan(int id) async {
    var connectivity = await Connectivity().checkConnectivity();

    if (connectivity == ConnectivityResult.wifi ||
        connectivity == ConnectivityResult.mobile) {
      if (!_cachedPlanes.containsKey(id)) {
        try {
          String itemURL = urlPlanes + "/" + id.toString();
          final response = await get(Uri.parse(itemURL));
          var jsonData = jsonDecode(response.body);
          jsonData = jsonData[0];
          var fields = jsonData['fields'];
          Plan plan = Plan(
              nombre: fields['nombre'],
              descripcion: fields['descripcion'],
              horaInicia: fields['horaInicia'].toString(),
              propietario: "NA",
              lugar: fields['lugar'].toString(),
              grupo: fields['grupo'].toString(),
              tipo: 'CI');
          fetchLugar(fields['lugar']);
          setState(() {
            _cachedPlanes[id] = plan;

            // _planesJson = _planesJson.map((plan) => {
            //   plan = plan.fields;
            // });
          });
        } catch (err) {
          print("error fetching planes: $err");
        }
      }
    } else {}
  }

  void fetchAllPlanes() async{
    var connectivity = await Connectivity().checkConnectivity();

    if (connectivity == ConnectivityResult.wifi ||
        connectivity == ConnectivityResult.mobile) {
      if (_cachedPlanes.isEmpty) {
        try {
          String itemURL = urlPlanes;
          final response = await get(Uri.parse(itemURL));
          var jsonData = jsonDecode(response.body);

          for(int i = 0; i<jsonData.length; i++){
            var fields = jsonData[i]['fields'];
            print(fields);
            Plan plan = Plan(
                nombre: fields['nombre'],
                descripcion: fields['descripcion'],
                horaInicia: fields['horaInicia'].toString(),
                propietario: "NA",
                lugar: fields['lugar'].toString(),
                grupo: fields['grupo'].toString(),
                tipo: 'CI');
            fetchLugar(fields['lugar']);
            setState(() {
              _cachedPlanes[i] = plan;
            });
          }


          //
        } catch (err) {
          print("error fetching data: $err");
        }
      }
    } else {}
  }

  void fetchLugar(int id) async {

    var connectivity = await Connectivity().checkConnectivity();

    if (connectivity == ConnectivityResult.wifi ||
        connectivity == ConnectivityResult.mobile) {
      if (!_cachedLugares.containsKey(id)) {
        try {
          String itemURL = urlLugares + "/" + id.toString();
          final response = await get(Uri.parse(itemURL));
          var jsonData = jsonDecode(response.body);
          jsonData = jsonData[0];
          var fields = jsonData['fields'];

          Lugar lugar = Lugar(
              nombre: fields['nombre'],
              precio: fields['precio'],
              direccion: fields['direccion'],
              tipoPlan: fields['tipo'],
              horaApertura: fields['horaApertura'],
              horaCierre: fields['horaCierre'],
              calificacion: fields['calificacion'],
              rutaImagen: fields['imagen'],
              latitud: double.parse(fields['latitud']),
              longitud: double.parse(fields['longitud']),
              );
          setState(() {
            _cachedLugares[id] = lugar;

            // _planesJson = _planesJson.map((plan) => {
            //   plan = plan.fields;
            // });
          });
        } catch (err) {
          print("error fetching data: $err");
        }
      }
    } else {}
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _fetchPlans(1);
    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        _getMoreData();
      }
    });
  }

  _fetchPlans(int a) {
    fetchAllPlanes();
  }

  _getIsConnected() async {
    var connectivity = await Connectivity().checkConnectivity();

    if (connectivity == ConnectivityResult.wifi ||
        connectivity == ConnectivityResult.mobile) {
      return true;
    } else {
      return false;
    }
  }

  Future<void> _getMoreData() async {
    _fetchPlans(_cachedPlanes.keys.toList().reduce(max));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).primaryColor,
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(
          Icons.add,
          color: Colors.white,
        ),
        onPressed: () {
          Navigator.of(context).push(
            MaterialPageRoute(builder: (context) => CrearPlanPage()),
          );
        },
        backgroundColor: Theme.of(context).primaryColor,
      ),
      bottomNavigationBar: NavigationBarWidget(),
      drawer: Drawer(
        backgroundColor: Theme.of(context).primaryColor,
        child: ListView(
          padding: EdgeInsets.all(20),
          children: [
            ListTile(
              title: const Text('Cambiar vista de inicio'),
              textColor: Colors.white,
              onTap: () {
                changeViewType();
              },
            ),

          ],
        )
      ),
      backgroundColor: Theme.of(context).scaffoldBackgroundColor,
      body: Container(
        child: RefreshIndicator(
          onRefresh: _getMoreData,
          child: Column(
            children: [
              SizedBox(
                height: 20,
              ),
              FutureBuilder(
                  future: _getIsConnected(),
                  builder: (context, snapshot) {
                    if (snapshot.hasData) {
                      if (snapshot.data == true) {
                        return Expanded(
                          child: ListView.builder(
                            controller: _scrollController,
                            itemCount: _cachedPlanes.length,
                            itemBuilder: (BuildContext bctx, int index) {
                              var keysPlanes = _cachedPlanes.keys.toList();
                              var plan = _cachedPlanes[keysPlanes[index]];

                              var rng = Random();
                              var typeN = rng.nextInt(3);
                              var type = "HO";
                              if (typeN == 0) {
                                type = "BI";
                              } else if (typeN == 1) {
                                type = "CI";
                              } else {
                                type = "RU";
                              }
                              var nnombre = plan?.nombre ?? '';
                              var ndescripcion = plan?.descripcion ?? '';
                              var nhoraInicia = plan?.horaInicia ?? '';
                              var nlugar = plan?.lugar ?? '1';
                              var lugar = _cachedLugares[int.parse(nlugar)];
                              var ngrupo = plan?.grupo ?? '';
                              var nruta = lugar?.rutaImagen ?? 'https://concepto.de/wp-content/uploads/2015/05/Default-1-e1547044737976.jpg';

                              var toReturn = InkWell(
                                onTap: (){Navigator.of(context).push(
                                    MaterialPageRoute(builder: (context) => DetallePlan(nombrePlan: nnombre, descripcionPlan: ndescripcion, urlImagenPlan: nruta, grupoPlan: ngrupo, horaIniciaPlan: nhoraInicia, lugarPlan: nlugar,)),);},
                                  child: PlanCard(
                                  plan: Plan(
                                      nombre: nnombre,
                                      descripcion: ndescripcion,
                                      horaInicia: nhoraInicia
                                          .toString()
                                          .split("T")[1]
                                          .split("Z")[0]
                                          .substring(0, 5),
                                      propietario: 'nadie',
                                      lugar: nlugar,
                                      grupo: ngrupo,
                                      tipo: type
                                  ),
                                imageURL: nruta,
                                bigCardSize: viewType,
                              ));

                              return toReturn;
                            },
                          ),
                        );
                      } else {
                        return RefreshIndicator(
                          onRefresh: _getMoreData,
                          child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text("No hay conexion"),
                                Container(
                                    width: 100, height: 30),
                                // IconButton(
                                //   icon: Icon(Icons.refresh_outlined),
                                //   onPressed: ()=>{
                                //     if(_getIsConnected())
                                //       {
                                //         print("sirve")
                                //       }
                                //
                                //   },
                                // ),
                              ],
                            ),
                          ],
                        )
                        );
                      }
                    }
                    return CircularProgressIndicator();
                  }),
            ],
          ),
        ),
      ),
    );
  }
}
