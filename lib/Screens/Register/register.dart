import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:parchapp/components/background.dart';

import '../dashboard.dart';

class Register extends StatefulWidget {
  const Register({Key? key}) : super(key: key);

  @override
  _RegisterState createState() => _RegisterState();
}

class _RegisterState extends State<Register> {
  var username = "";
  var name = "";
  var password = "";
  var rePassword = "";
  var email = "";
  var fechaNacimiento = DateTime.now();
  var fechaString = "${DateTime.now().day}-${DateTime.now().month}-${DateTime.now().year}";
  var telefono = "";
  final imagen = "https://www.pngkey.com/png/detail/950-9501315_katie-notopoulos-katienotopoulos-i-write-about-tech-user.png";
  final url = "http://44.198.195.210:8080/clientes";

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Background(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.symmetric(horizontal: 40),
              child: Text(
                "Bienvenido a ParchApp",
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Color(0xFF412582),
                    fontSize: 28),
                textAlign: TextAlign.left,
              ),
            ),
            Container(
              alignment: Alignment.centerLeft,
              margin: EdgeInsets.symmetric(horizontal: 40, vertical: 2),
              child: Text(
                "Ingresa tus credenciales",
                style: TextStyle(fontSize: 12, color: Color(0xFF412582)),
              ),
            ),
            SizedBox(
              height: size.height * 0.03,
            ),
            Container(
              alignment: Alignment.center,
              margin: EdgeInsets.symmetric(horizontal: 40),
              child: TextField(
                onChanged: (text) {
                  username = text;
                },
                decoration: InputDecoration(labelText: "Nombre de usuario"),
              ),
            ),
            Container(
              alignment: Alignment.center,
              margin: EdgeInsets.symmetric(horizontal: 40),
              child: TextField(
                onChanged: (text) {
                  name = text;
                },
                decoration: InputDecoration(labelText: "Nombre"),
              ),
            ),
            Container(
              alignment: Alignment.center,
              margin: EdgeInsets.symmetric(horizontal: 40),
              child: TextField(
                onChanged: (text) {
                  email = text;
                },
                decoration: InputDecoration(labelText: "Correo electronico"),
              ),
            ),
            Container(
              alignment: Alignment.center,
              margin: EdgeInsets.symmetric(horizontal: 40),
              child: TextField(
                onChanged: (text) {
                  password = text;
                },
                decoration: InputDecoration(labelText: "Contraseña"),
              ),
            ),
            Container(
              alignment: Alignment.center,
              margin: EdgeInsets.symmetric(horizontal: 40),
              child: TextField(
                onChanged: (text) {
                  rePassword = text;
                },
                decoration: InputDecoration(labelText: "Confirma tu contraseña"),
              ),
            ),
            SizedBox(
              height: size.height * 0.03,
            ),
            Container(
              alignment: Alignment.centerLeft,
              margin: EdgeInsets.symmetric(horizontal: 40),
              child: Text("Fecha de Nacimiento")
            ),
            Container(
                alignment: Alignment.centerLeft,
                margin: EdgeInsets.symmetric(horizontal: 40),
                child: TextButton(
                  onPressed: () {
                    _selectDate(context);
                  },
                  child: Text(fechaString),
                )
            ),
            Container(
              alignment: Alignment.center,
              margin: EdgeInsets.symmetric(horizontal: 40),
              child: TextField(
                onChanged: (text) {
                  telefono = text;
                },
                decoration: InputDecoration(labelText: "Numero de Telefono"),
              ),
            ),
            Container(
              alignment: Alignment.center,
              margin: EdgeInsets.symmetric(horizontal: 40, vertical: 10),
              child: RaisedButton(
                onPressed: () {
                  registerRequest(username, name, password, rePassword, email, fechaString, telefono)
                  .then((response) => {
                    if (response == "Registro exitoso") {
                      _goToDashboard(context)
                    } else {
                      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                        content: Text(response),
                      ))
                    }
                  });
                },
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(80.0)),
                textColor: Colors.black,
                child: Container(
                  alignment: Alignment.center,
                  height: 50.0,
                  width: size.width * 0.5,
                  child: Text(
                    "REGISTRARSE",
                    textAlign: TextAlign.center,
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _goToDashboard(BuildContext context) async {
    final route = MaterialPageRoute(builder: (BuildContext context) {
      return Dashboard();
    });
    Navigator.of(context).push(route);
  }

  Future<String> registerRequest(String username1, String name1, String password1,
                                 String rePassword1, String email1, String fechaNacimiento1,
                                 String telefono1) async {
    try {
      if (password1 != rePassword1) {
        return "Las contraseñas no coinciden";
      }
      if (password1.length < 8) {
        return "La constraseña debe tener al menos 8 caracteres";
      }
      if (username1.length < 8) {
        return "El nombre de usuario debe tener al menos 8 caracteres";
      }
      if (name1.length < 1) {
        return "El nombre no puede ser vacio";
      }
      if (!email1.contains("@") || !email1.contains(".com")) {
        return "Correo invalido";
      }
      if (fechaNacimiento.year > 2008) {
        return "Debes tener al menos 14 años para utilizar parchapp";
      }
      if (telefono1.length < 8) {
        return "Telefono invalido";
      }
      var postBody = jsonEncode(<String, String> {
        "username": username1,
        "nombre": name1,
        "password": password1,
        "correo": email1,
        "fechaNacimiento": fechaNacimiento1,
        "telefono": telefono1,
        "imagen": imagen
      });
      final response = await http.post(Uri.parse(url), body: postBody);
      if (response.statusCode == 200) {
        return "Registro exitoso";
      } else {
        return response.body;
      }
    } catch(e) {
      return "Error al registrarse";
    }
  }

  _selectDate(BuildContext context) async {
    final DateTime? selected = await showDatePicker(
      context: context,
      initialDate: fechaNacimiento,
      firstDate: DateTime(1950),
      lastDate: DateTime(2025),
    );
    if (selected != null && selected != fechaNacimiento) {
      fechaNacimiento = selected;
      setState(() {
        fechaString = "${fechaNacimiento.day}-${fechaNacimiento.month}-${fechaNacimiento.year}";
      });
    }
  }
}