import 'dart:async';

import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:parchapp/model/map_marker.dart';
import 'package:external_app_launcher/external_app_launcher.dart';

class MapPage extends StatefulWidget {
  @override
  State<MapPage> createState() => MapPageState();
}

class MapPageState extends State<MapPage> {
  Completer<GoogleMapController> _controller = Completer();

  static final CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(4.602221555701779, -74.06515104254576),
    zoom: 15.4746,
  );

  final _myLocation = LatLng(4.602221555701779, 4.602221555701779);

  static final CameraPosition _kPlan = CameraPosition(
      bearing: 192.8334901395799,
      target: LatLng(4.669794994944934, -74.05489392902221),
      tilt: 59.440717697143555,
      zoom: 19.151926040649414);

  List<Marker> _buildMarkers() {
    final _markerList = <Marker>[];
    for (int i = 0; i < mapMarkers.length; i++) {
      final mapItem = mapMarkers[i];
      _markerList.add(
        Marker(
            markerId: MarkerId(mapItem.toString()), position: mapItem.location),
      );
    }
    return _markerList;
  }

  Set<Marker> _createMarker() {
    return <Marker>[
      Marker(
        markerId: MarkerId('home'),
        position: LatLng(4.669794994944934, -74.05489392902221),
      )
    ].toSet();
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    final _markers = _buildMarkers();
    final item = mapMarkers[0];
    return new Scaffold(
        appBar: AppBar(
          title: Text("Encuentra planes cerca"),
          backgroundColor: Theme.of(context).primaryColor,
        ),
        body: Stack(
          children: [
            GoogleMap(
              mapType: MapType.normal,
              initialCameraPosition: _kGooglePlex,
              markers: Set.of(_markers),
              onMapCreated: (GoogleMapController controller) {
                _controller.complete(controller);
              },
            ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(),
                  ],
                )
              ],
            ),
            Container(
              alignment: Alignment.topRight,
              margin: EdgeInsets.symmetric(horizontal: 40, vertical: 10),
              child: TextButton(
                  onPressed: () async {
                    await LaunchApp.openApp(
                      androidPackageName: 'com.ubercab'
                    );
                  },
                  style: ButtonStyle(
                    backgroundColor: MaterialStateProperty.all<Color>(Colors.orange)
                  ),
                  child: Text("Uber")),
            ),
          ],
        ),
        floatingActionButton: FloatingActionButton(
          onPressed: _goToPlan,
          child: Icon(
            Icons.alt_route,
            color: Colors.white,
          ),
          backgroundColor: Theme.of(context).primaryColor,
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.startFloat);
  }

  Future<void> _goToPlan() async {
    final GoogleMapController controller = await _controller.future;
    controller.animateCamera(CameraUpdate.newCameraPosition(_kPlan));
  }
}

class _MapItemDetails extends StatelessWidget {
  const _MapItemDetails({
    Key? key,
    required this.mapMaker,
  }) : super(key: key);

  final MapMaker mapMaker;
  @override
  Widget build(BuildContext context) {
    final _styleTitle = TextStyle(
        color: Colors.black, fontSize: 20, fontWeight: FontWeight.bold);
    final _styleAddress = TextStyle(color: Colors.grey[800], fontSize: 20);
    return Padding(
      padding: const EdgeInsets.all(15.0),
      child: Card(
        color: Colors.white,
        child: Row(
          children: [
            Expanded(
              child: Image.asset(mapMaker.image),
            ),
            Expanded(
                child: Column(
                  children: [
                    Text(
                      mapMaker.title,
                      style: _styleTitle,
                    ),
                    const SizedBox(
                      height: 10,
                    ),
                  ],
                ))
          ],
        ),
      ),
    );
  }
}
