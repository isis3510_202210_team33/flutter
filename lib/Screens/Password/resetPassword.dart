import 'package:flutter/material.dart';
import 'package:parchapp/components/background.dart';
import 'package:parchapp/Screens/Login/login.dart';

class PasswordReset extends StatefulWidget {
  const PasswordReset({Key? key}) : super(key: key);

  @override
  _PasswordResetState createState() => _PasswordResetState();
}

class _PasswordResetState extends State<PasswordReset> {

  var password = "";
  var repassword = "";

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery
        .of(context)
        .size;
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Background(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.symmetric(horizontal: 40),
              child: Text(
                "Reestablecer contraseña",
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Color(0xFF412582),
                    fontSize: 28),
                textAlign: TextAlign.left,
              ),
            ),
            Container(
              alignment: Alignment.centerLeft,
              margin: EdgeInsets.symmetric(horizontal: 40, vertical: 2),
              child: Text(
                "Ingrese su nueva contraseña",
                style: TextStyle(fontSize: 12, color: Color(0xFF412582)),
              ),
            ),
            SizedBox(
              height: size.height * 0.03,
            ),
            Container(
              alignment: Alignment.center,
              margin: EdgeInsets.symmetric(horizontal: 40),
              child: TextField(
                onChanged: (text) {
                  password = text;
                },
                decoration: InputDecoration(labelText: "Nueva contraseña"),
              ),
            ),
            Container(
              alignment: Alignment.center,
              margin: EdgeInsets.symmetric(horizontal: 40),
              child: TextField(
                onChanged: (text) {
                  repassword = text;
                },
                decoration: InputDecoration(labelText: "Confirmar nueva contraseña"),
              ),
            ),
            Container(
              alignment: Alignment.center,
              margin: EdgeInsets.symmetric(horizontal: 40, vertical: 10),
              child: RaisedButton(
                  onPressed: () {
                    String message = validatePassword(password);
                    if (message == "Las contraseñas no coinciden" || message == "La constraseña debe tener al menos 8 caracteres") {
                      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                        content: Text(
                          message,
                          style: const TextStyle(
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ));
                    } else {
                      _goToLogin(context);
                      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                        content: Text(
                          message,
                          style: const TextStyle(
                            fontWeight: FontWeight.bold,
                          ),),
                      ));
                    }
                  },
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(80.0)),
                  child: Text("Reestablecer contraseña")),
            ),
            SizedBox(
              height: size.height * 0.05,
            ),
          ],
        ),
      ),
    );
  }
  void _goToLogin(BuildContext context) {
    final route = MaterialPageRoute(builder: (BuildContext context) {
      return Login();
    });
    Navigator.of(context).push(route);
  }

  String validatePassword(pass) {
    if (password != repassword) {
      return "Las contraseñas no coinciden";
    }
    else if (password.length < 8) {
      return "La constraseña debe tener al menos 8 caracteres";
    }
    else {
      return "Contraseña reestablecida correctamente";
    }
  }

  void _goToPasswordReset(BuildContext context) {
    final route = MaterialPageRoute(builder: (BuildContext context) {
      return Login();
    });
    Navigator.of(context).push(route);
  }
}