import 'package:flutter/material.dart';
import 'package:parchapp/components/background.dart';
import 'package:parchapp/Screens/Login/login.dart';
import 'package:parchapp/Screens/Password/resetPassword.dart';

class Password extends StatefulWidget {
  const Password({Key? key}) : super(key: key);

  @override
  _PasswordState createState() => _PasswordState();
}

class _PasswordState extends State<Password> {

  var email = "";

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery
        .of(context)
        .size;
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Background(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.symmetric(horizontal: 40),
              child: Text(
                "Reestablecer contraseña",
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Color(0xFF412582),
                    fontSize: 28),
                textAlign: TextAlign.left,
              ),
            ),
            Container(
              alignment: Alignment.centerLeft,
              margin: EdgeInsets.symmetric(horizontal: 40, vertical: 2),
              child: Text(
                "Ingresa el correo electronico de tu cuenta",
                style: TextStyle(fontSize: 12, color: Color(0xFF412582)),
              ),
            ),
            SizedBox(
              height: size.height * 0.03,
            ),
            Container(
              alignment: Alignment.center,
              margin: EdgeInsets.symmetric(horizontal: 40),
              child: TextField(
                onChanged: (text) {
                  email = text;
                },
                decoration: InputDecoration(labelText: "Correo Electronico"),
              ),
            ),
            Container(
              alignment: Alignment.center,
              margin: EdgeInsets.symmetric(horizontal: 40, vertical: 10),
              child: RaisedButton(
                  onPressed: () {
                    if (!validateEmail(email)) {
                      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                        content: Text(
                          "Email Invalido",
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          ),),
                      ));
                    } else {
                      _goToLogin(context);
                      ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                        content: Text(
                          "Email de recuperacion enviado",
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                          ),),
                      ));
                    }
                  },
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(80.0)),
                  child: Text("Enviar Correo ")),
            ),
            SizedBox(
              height: size.height * 0.05,
            ),
            Container(
              alignment: Alignment.bottomRight,
              margin: EdgeInsets.symmetric(horizontal: 40, vertical: 10),
              child: TextButton(
                onPressed: () {
                  _goToPasswordReset(context);
                },
                child: Text(
                  "Dev Only"
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
  void _goToLogin(BuildContext context) {
    final route = MaterialPageRoute(builder: (BuildContext context) {
      return Login();
    });
    Navigator.of(context).push(route);
  }

  bool validateEmail(email) {
    if (!email.contains("@") || !email.contains(".com")) {
      return false;
    } else {
      return true;
    }
  }

  void _goToPasswordReset(BuildContext context) {
    final route = MaterialPageRoute(builder: (BuildContext context) {
      return PasswordReset();
    });
    Navigator.of(context).push(route);
  }
}