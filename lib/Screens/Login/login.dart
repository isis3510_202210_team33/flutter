import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:parchapp/components/background.dart';
import 'package:parchapp/Screens/Profile/profile_page.dart';
import 'package:parchapp/Screens/Register/register.dart';
import 'package:parchapp/Screens/Password/password.dart';
import 'package:parchapp/model/user.dart';
import 'package:parchapp/globals.dart' as globals;

import '../dashboard.dart';

var username = "";
var password = "";

class Login extends StatelessWidget {
  const Login({Key? key}) : super(key: key);

  final url = "http://44.198.195.210:8080/login/cliente";

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Background(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Container(
              alignment: Alignment.centerLeft,
              padding: EdgeInsets.symmetric(horizontal: 40),
              child: Text(
                "Bienvenido a ParchApp",
                style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Color(0xFF412582),
                    fontSize: 28),
                textAlign: TextAlign.left,
              ),
            ),
            Container(
              alignment: Alignment.centerLeft,
              margin: EdgeInsets.symmetric(horizontal: 40, vertical: 2),
              child: Text(
                "Ingresa tus credenciales",
                style: TextStyle(fontSize: 12, color: Color(0xFF412582)),
              ),
            ),
            SizedBox(
              height: size.height * 0.03,
            ),
            Container(
              alignment: Alignment.center,
              margin: EdgeInsets.symmetric(horizontal: 40),
              child: TextField(
                onChanged: (text) {
                  username = text;
                },
                decoration: InputDecoration(labelText: "Nombre de usuario"),
              ),
            ),
            SizedBox(
              height: size.height * 0.03,
            ),
            Container(
              alignment: Alignment.center,
              margin: EdgeInsets.symmetric(horizontal: 40),
              child: TextField(
                onChanged: (text) {
                  password = text;
                },
                decoration: InputDecoration(labelText: "Contraseña"),
                obscureText: true,
              ),
            ),
            Container(
              alignment: Alignment.centerRight,
              margin: EdgeInsets.symmetric(horizontal: 40, vertical: 10),
              child: TextButton(
                onPressed: () {
                  _goToPassword(context);
                },
                child: Text("Reestablecer constraseña",
                style: TextStyle(fontSize: 12, color: Color(0xFF412582))
                ),
              ),
            ),
            Container(
              alignment: Alignment.centerRight,
              margin: EdgeInsets.symmetric(horizontal: 40, vertical: 10),
              child: TextButton(
                  onPressed: () {
                    _goToRegister(context);
                  },
                  child: Text("Registrate")),
            ),
            SizedBox(
              height: size.height * 0.05,
            ),
            Container(
              alignment: Alignment.center,
              margin: EdgeInsets.symmetric(horizontal: 40, vertical: 10),
              child: RaisedButton(
                onPressed: () {
                  loginRequest(username, password).then((response) => {
                        if (response == "Inicio de sesion exitoso")
                          {_goToDashboard(context)}
                        else
                          {
                            ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                              content: Text(response),
                            ))
                          }
                      });
                },
                onLongPress: () {
                  _goToProfilePage(context);
                },
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(80.0)),
                textColor: Colors.black,
                child: Container(
                  alignment: Alignment.center,
                  height: 50.0,
                  width: size.width * 0.5,
                  child: Text(
                    "LOGIN",
                    textAlign: TextAlign.center,
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  void _goToDashboard(BuildContext context) {
    final route = MaterialPageRoute(builder: (BuildContext context) {
      return Dashboard();
    });
    Navigator.of(context).push(route);
  }

  void _goToProfilePage(BuildContext context) {
    final route = MaterialPageRoute(builder: (BuildContext context) {
      return ProfilePage();
    });
    Navigator.of(context).push(route);
  }

  void _goToRegister(BuildContext context) {
    final route = MaterialPageRoute(builder: (BuildContext context) {
      return Register();
    });
    Navigator.of(context).push(route);
  }

  void _goToPassword(BuildContext context) {
    final route = MaterialPageRoute(builder: (BuildContext context) {
      return Password();
    });
    Navigator.of(context).push(route);
  }

  Future<String> loginRequest(String username, String password) async {
    try {
      var postBody = jsonEncode(
          <String, String>{"username": username, "password": password});
      final response = await http.post(Uri.parse(url), body: postBody);
      if (response.statusCode == 200) {
        var jsonData = jsonDecode(response.body);
        // print(jsonData);
        jsonData = jsonData[0];
        print(jsonData['pk']);
        var info = jsonData['fields'];

        User sesion = User(
            imagePath: 'https://storage.googleapis.com/media.clinicavisualyauditiva.com/images/2019/11/211fd983-default-user-image.png',
            name: info['nombre'],
            userName: info['username'],
            birthday: info['fechaNacimiento'],
            email: info['correo'],
            password: info['password'],
            friends: info['amigos'],
            groups: info['grupos'],
            planes: info['planes'],
            isDarkMode: false
        );

        globals.sesion.setInstanceUsuario(sesion);
        globals.sesion.setIdUser(jsonData['pk']);
        return "Inicio de sesion exitoso";
      } else {
        return response.body;
      }
    } catch (e) {
      return "Error al iniciar sesion";
    }
  }
}
