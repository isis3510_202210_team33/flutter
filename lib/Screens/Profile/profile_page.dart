import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:parchapp/Screens/Friends/friends_page.dart';
import 'package:parchapp/Screens/Profile/edit_profile_page.dart';
import 'package:parchapp/model/user.dart';
import 'package:parchapp/utils/user_preferences.dart';
import 'package:parchapp/widget/appbar_widget.dart';
import 'package:parchapp/widget/button_widget.dart';
import 'package:parchapp/widget/profile_widget.dart';
import 'package:parchapp/widget/number_widget.dart';
import 'package:parchapp/widget/change_theme_button_widget.dart.dart';

import '../Groups/groups_page.dart';

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  @override
  Widget build(BuildContext context) {
    final user = UserPreferences.getUser();

    return Scaffold(
        appBar: AppBar(
          actions: [
            ChangeThemeButtonWidget(),
          ],
          backgroundColor: Theme.of(context).primaryColor,
        ),
        body: ListView(
          physics: BouncingScrollPhysics(),
          children: [
            const SizedBox(height: 24),
            ProfileWidget(
              imagePath: user.imagePath,
              onClicked: () async {
                await Navigator.of(context).push(
                  MaterialPageRoute(builder: (context) => EditProfilePage()),
                );
                setState(() {});
              },
            ),
            const SizedBox(height: 24),
            buildName(user),
            const SizedBox(height: 24),
            buildBirthday(user),
            const SizedBox(height: 24),
            Center(child: buildUpgradeButton()),
            const SizedBox(height: 24),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                ClipRRect(
                  borderRadius: BorderRadius.circular(20),
                  child: Container(
                    width: 100,
                    height: 100,
                    color: Theme.of(context).primaryColor,
                    child: Ink(
                      child: InkWell(
                        onTap: () {
                          Navigator.of(context).push(
                            MaterialPageRoute(builder: (context) => FriendsPage()),
                          );
                        },
                        child: const Align(
                          child: Padding(
                            padding: EdgeInsets.all(10.0),
                            child: Text(
                              'AMIGOS',
                              style: TextStyle(
                                fontSize: 12,
                                fontWeight: FontWeight.w900,
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  width: 30,
                ),
                ClipRRect(
                  borderRadius: BorderRadius.circular(20),
                  child: Container(
                    width: 100,
                    height: 100,
                    color: Theme.of(context).primaryColor,
                    child: Ink(
                      child: InkWell(
                        onTap: () {
                          Navigator.of(context).push(
                            MaterialPageRoute(builder: (context) => GroupsPage()),
                          );
                        },
                        child: const Align(
                          child: Padding(
                            padding: EdgeInsets.all(10.0),
                            child: Text(
                              'GRUPOS',
                              style: TextStyle(
                                fontSize: 12,
                                fontWeight: FontWeight.w900,
                                color: Colors.white,
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),
              ],
            )
          ],
        ));
  }

  Widget buildName(User user) => Column(
        children: [
          Text(
            user.userName,
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24),
          ),
          const SizedBox(height: 4),
          Text(
            user.name,
            style: TextStyle(color: Colors.grey),
          )
        ],
      );

  Widget buildBirthday(User user) => Column(
        children: [
          Text(
            'Birthday',
            style: TextStyle(fontWeight: FontWeight.bold, fontSize: 24),
          ),
          const SizedBox(height: 4),
          Text(
            user.birthday,
            style: TextStyle(color: Colors.grey),
          )
        ],
      );

  Widget buildUpgradeButton() => ButtonWidget(
        text: 'Calendario',
        onClicked: () {},
      );
}
