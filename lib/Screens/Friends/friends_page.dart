import 'package:flutter/material.dart';

class FriendsPage extends StatelessWidget {
  FriendsPage({Key? key}) : super(key: key);
  final amigos = ['Frida Kahlo', 'Dante Alighieri', 'Nicolas Calero', 'Jaime Torres', 'Jhorman Caballero'];
  final amigoCardHeight = 65.0;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Amigos"),
        backgroundColor: Theme.of(context).primaryColor,
      ),
      body: Column(
        children: [

          Expanded(
            child: ListView.builder(
              itemCount: amigos.length,
              itemBuilder: (BuildContext bctx, int index) {
                var toReturn = Container(
                  margin: EdgeInsets.all(15),
                  height: amigoCardHeight,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                    color: Theme.of(context).scaffoldBackgroundColor,
                    boxShadow: [
                      BoxShadow(
                        color: Theme.of(context).primaryColor,
                        offset: Offset(0.0, 1.0), //(x,y)
                        blurRadius: 8.0,
                      ),
                    ],
                  ),
                  child: Stack(
                    children: [
                      Stack(
                        children: [
                          Positioned.fill(
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(20),

                            ),
                          ),
                        ],
                      ),
                      Positioned(
                        bottom: 0,
                        left: 0,
                        right: 0,
                        child: Container(
                          height: amigoCardHeight,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(
                                bottomLeft: Radius.circular(20),
                                bottomRight: Radius.circular(20),
                                topLeft: Radius.circular(20),
                                topRight: Radius.circular(20)),
                            gradient: LinearGradient(
                                begin: Alignment.centerLeft,
                                end: Alignment.centerRight,
                                colors: [
                                  Theme.of(context)
                                      .primaryColor
                                      .withOpacity(1),
                                  Theme.of(context)
                                      .primaryColor
                                      .withOpacity(0.8)
                                ]),
                          ),
                        ),
                      ),
                      Container(
                          alignment: Alignment.center,
                          child: Container(
                            alignment: Alignment.bottomCenter,
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.center,
                              mainAxisAlignment:
                              MainAxisAlignment.spaceAround,
                              children: [
                                SizedBox(
                                  width: 170,
                                  child: Padding(
                                    padding: const EdgeInsets.only(
                                        top: 20, bottom: 20),
                                    child: Column(
                                      mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                      crossAxisAlignment:
                                      CrossAxisAlignment.start,
                                      children: [
                                        Text(
                                          amigos[index],
                                          style: TextStyle(
                                            color: Colors.white,
                                            fontSize: 20,
                                          ),
                                        ),

                                        // Text(
                                        //   "Creado por " +
                                        //       plan.propietario,
                                        //   style: TextStyle(
                                        //     color: Colors.white,
                                        //     fontSize: 12,
                                        //   ),
                                        // ),
                                      ],
                                    ),
                                  ),
                                ),
                                Column(
                                  mainAxisAlignment:
                                  MainAxisAlignment.spaceAround,
                                  children: [
                                    SizedBox(
                                      //La sized box correspondiente a la imagen del lugar
                                      width: 60,
                                      height: 60,
                                      child: Stack(
                                        children: [
                                          Positioned.fill(
                                            child: ClipRRect(
                                              borderRadius:
                                              BorderRadius.circular(20),
                                              child: Image.asset(
                                                'assets/user.png',
                                                fit: BoxFit.cover,
                                              ),
                                            ),
                                          )
                                        ],
                                      ),
                                    ),

                                  ],
                                )
                              ],
                            ),
                          ))
                    ],
                  ),
                );

                return toReturn;
              },
            ),
          ),
        ],
      ),
    );
  }
}
