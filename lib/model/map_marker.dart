import 'package:google_maps_flutter/google_maps_flutter.dart';

class MapMaker {
  const MapMaker({
    required this.image,
    required this.title,
    required this.address,
    required this.location,
  });
  final String image;
  final String title;
  final String address;
  final LatLng location;
}

final _locations = [
  LatLng(4.669794994944934, -74.05489392902221),
  LatLng(4.664798964944934, -74.05489392902421),
  LatLng(4.664798964944934, -74.05789392902421),
];

const _path = 'assets/animated_markers_map/';

final mapMarkers = [
  MapMaker(
    image: '${_path}logo_marcos.png',
    title: 'Marcos',
    address: 'Direccion Marcos',
    location: _locations[0],
  ),
  MapMaker(
    image: '${_path}logo_corral.png',
    title: 'El corral',
    address: 'Direccion el corral',
    location: _locations[1],
  ),
  MapMaker(
    image: '${_path}logo_mac.jpg',
    title: 'Mac',
    address: 'Direccion Mac',
    location: _locations[2],
  ),
];
