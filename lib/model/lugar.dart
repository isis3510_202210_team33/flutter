class Lugar {
  String nombre;
  double precio;
  String direccion;
  String tipoPlan;
  String horaApertura;
  String horaCierre;
  double calificacion;
  String rutaImagen;
  double latitud;
  double longitud;

  Lugar({
    required this.nombre,
    required this.precio,
    required this.direccion,
    required this.tipoPlan,
    this.horaApertura = "0",
    this.horaCierre = "86400",
    this.calificacion = 3,
    this.rutaImagen = "",
    required this.latitud,
    required this.longitud,
  });
}
