class User {
  final String imagePath;
  final String name;
  final String userName;
  final String birthday;
  final String email;
  final String password;
  final List friends;
  final List groups;
  final List planes;
  final bool isDarkMode;

  const User({
    required this.imagePath,
    required this.name,
    required this.userName,
    required this.birthday,
    required this.email,
    required this.password,
    required this.friends,
    required this.groups,
    required this.planes,
    required this.isDarkMode,
  });

  User copy({
    String? imagePath,
    String? name,
    String? userName,
    String? birthday,
    String? email,
    String? password,
    int? friends,
    int? groups,
    int? planes,
    bool? isDarkMode,
  }) =>
      User(
        imagePath: imagePath ?? this.imagePath,
        name: name ?? this.name,
        userName: userName ?? this.userName,
        birthday: birthday ?? this.birthday,
        email: email ?? this.email,
        password: password ?? this.password,
        friends: [],
        groups: [],
        planes: [],
        isDarkMode: isDarkMode ?? this.isDarkMode,
      );

  static User fromJson(Map<String, dynamic> json) => User(
        imagePath: json['imagePath'],
        name: json['name'],
        userName: json['userName'],
        birthday: json['birthday'],
        email: json['email'],
        password: json['password'],
        friends: json['friends'],
        groups: json['groups'],
        planes: json['planes'],
        isDarkMode: json['isDarkMode'],
      );

  Map<String, dynamic> toJson() => {
        "imagePath": imagePath,
        "name": name,
        "userName": userName,
        "birthday": birthday,
        "email": email,
        "password": password,
        "friends": friends,
        "groups": groups,
        "planes": planes,
        "isDarkMode": isDarkMode,
      };
}
