class Plan {
  final String nombre;
  final String descripcion;
  final double presupuesto;
  final String horaInicia;
  final String propietario;
  final String grupo;
  final String lugar;
  final String tipo;


  const Plan(
      {required this.nombre,
      required this.descripcion,
      this.presupuesto = 0,
      required this.horaInicia,
      required this.propietario,
      required this.lugar,
      required this.grupo,
      required this.tipo});
}
