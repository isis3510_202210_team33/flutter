import 'package:parchapp/model/user.dart';

class LoginInstance{

  late User usuario;
  late int idUser;

  LoginInstance(User pUsuario){
    usuario = pUsuario;
  }

  int getIdUser(){
    return idUser;
  }

  void setIdUser(int newIdUser){
    idUser = newIdUser;
  }

  User getInstanceUsuario(){
    return usuario;
  }

  void setInstanceUsuario(User nuevoUsuario){
    usuario = nuevoUsuario;
  }

}