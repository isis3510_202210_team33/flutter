import 'package:parchapp/model/plan.dart';

class PlanesMocks{
  static List<Plan> getMocksPlanes(){
    return[
      const Plan(
          nombre: "Salida exotica",
          descripcion: "Salida random para celebrar evento random en un lugar x.",
          horaInicia: "20:00",
          propietario: "Jaime",
          lugar: "Kahoot Bar",
          grupo: "Dream Team",
          tipo: "RU"
      ),
      const Plan(
          nombre: "The Batman",
          descripcion: "Vamos a ver la nueva pelicula de batman.",
          horaInicia: "21:30",
          propietario: "Nicolas",
          lugar: "Cineco Unicentro",
          grupo: "BCAGA",
          tipo: "CI"
      ),
      const Plan(
          nombre: "Sesion de estudio",
          descripcion: "Quien para estudiar en la biblioteca.",
          horaInicia: "12:00",
          propietario: "Santiago",
          lugar: "Biblioteca ML",
          grupo: "Dream Team 2",
          tipo: "BI"
      ),
    ];
  }
}