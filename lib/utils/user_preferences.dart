import 'dart:convert';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:parchapp/model/user.dart';
import 'package:parchapp/globals.dart' as globals;
class UserPreferences {
  static late SharedPreferences _preferences;

  static const _keyUser = 'user';
  static var myUser = User(
    imagePath: globals.sesion.getInstanceUsuario().imagePath,
    name: globals.sesion.getInstanceUsuario().name,
    userName: globals.sesion.getInstanceUsuario().userName,
    birthday: globals.sesion.getInstanceUsuario().birthday,
    email: globals.sesion.getInstanceUsuario().email,
    password: globals.sesion.getInstanceUsuario().password,
    friends: [],
    groups: [],
    planes: [],
    isDarkMode: false,
  );

  static Future init() async =>
      _preferences = await SharedPreferences.getInstance();

  static Future setUser(User user) async {
    final json = jsonEncode(user.toJson());

    await _preferences.setString(_keyUser, json);
  }

  static User getUser() {
    final json = _preferences.getString(_keyUser);

    return json == null ? myUser : User.fromJson(jsonDecode(json));
  }
}
