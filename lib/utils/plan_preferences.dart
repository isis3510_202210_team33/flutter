import 'package:parchapp/model/plan.dart';

class PlanPreferences {
  static const myPlan = Plan(
    nombre: 'Salida pre finales',
    descripcion: 'Vamos a celebrar el final de semestre como se debe, con el mejor ambiente y la mejor musica.',
    horaInicia: "20:00",
    propietario: "cale",
    grupo: "Dream Team",
    lugar: "Kahoot Bar",
    tipo: "Cine"

  );
}
