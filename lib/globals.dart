library parchapp.globals;

import 'package:parchapp/model/login_instance.dart';
import 'package:parchapp/model/user.dart';

User usuarioSesion = User(imagePath: "", name: "Nombre", userName: "Usuario", birthday: "", email: "", password: "", friends: [], groups: [], planes: [], isDarkMode: false);
LoginInstance sesion = LoginInstance(usuarioSesion);